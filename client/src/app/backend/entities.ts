export interface IBackendData {
  [channel: string]: string;
}

export interface IHealthCheck {
  alive: boolean;
}

export enum DataType {
  String = 's#',
  Integer = 'i#',
  Float = 'f#',
  Boolean = 'b#'
}

export interface INewValue {
  channel: string;
  stringValue: string;
}

export class BackendData {
  private readonly fields: string[];
  private readonly fieldMap: Map<string, string>;
  private readonly lastCollectedDate: Date;

  constructor(private readonly data: IBackendData) {
    this.lastCollectedDate = new Date();
    this.fields = [];
    this.fieldMap = new Map<string, string>();
    Object.keys(data)
      .forEach(k => {
        const reducedKey = k.substring(k.indexOf('#') + 1);
        this.fields.push(reducedKey);
        this.fieldMap.set(reducedKey, k);
      });
    this.fields.sort();
  }

  private getRawData(key: string, defaultValue: any = undefined): string | undefined {
    return key in this.data ? this.data[key] : defaultValue;
  }

  private getData(type: DataType, key: string, defaultValue: any = undefined): any | undefined {
    const fullKey = `${type}${key}`;
    const rawData = this.getRawData(fullKey);
    switch (type) {
      case DataType.String:
        return rawData !== undefined ? rawData : defaultValue;
      case DataType.Boolean:
        return rawData !== undefined ? rawData === '1' : defaultValue;
      case DataType.Integer:
        return rawData !== undefined ? Number.parseInt(rawData, 10) : defaultValue;
      case DataType.Float:
        return rawData !== undefined ? Number.parseFloat(rawData) : defaultValue;
      default:
        throw new Error(`Data Type ${type} is unknown!`);
    }
  }

  public get lastCollected(): Date {
    return this.lastCollectedDate;
  }

  public get hasAnyData(): boolean {
    return this.keys.length > 0;
  }

  public getDataType(key: string): DataType | undefined {
    if (!this.fieldMap.has(key)) {
      return undefined;
    }
    const fullKey = this.fieldMap.get(key)!;
    const type = fullKey.substring(0, fullKey.indexOf('#') + 1)
    return type as DataType;
  }

  public getFieldValue(key: string): any | undefined {
    if (!this.fieldMap.has(key)) {
      return undefined;
    }
    const fullKey = this.fieldMap.get(key)!;
    return this.getRawData(fullKey);
  }

  public get keys(): string[] {
    return this.fields;
  }

  public getString(key: string): string {
    return this.getData(DataType.String, key);
  }

  public getBoolean(key: string): boolean {
    return this.getData(DataType.Boolean, key);
  }

  public getInteger(key: string): number {
    return this.getData(DataType.Integer, key);
  }

  public getFloat(key: string): number {
    return this.getData(DataType.Float, key);
  }
}
