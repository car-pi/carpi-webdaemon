import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { hasNumberValue } from 'src/app/utils/number.utils';

@Component({
  selector: 'app-simple-widget',
  templateUrl: './simple-widget.component.html',
  styleUrls: ['./simple-widget.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class SimpleWidgetComponent {

  @Input() label: string | undefined;
  @Input() value: any | undefined;
  @Input() defaultValue: any | undefined;
  @Input() unit: string | undefined;
  @Input() numberFormat: string | undefined;
  @Input() prefix: string | undefined;

  constructor() { }

  get hasValue(): boolean {
    if (this.numberFormat) {
      return hasNumberValue(this.value);
    }
    return this.value !== undefined;
  }
  get hasDefaultValue(): boolean {
    return this.defaultValue !== undefined;
  }

  get hasPrefix(): boolean {
    return this.prefix !== undefined;
  }

}
