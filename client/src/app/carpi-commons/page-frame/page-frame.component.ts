import { Component, ChangeDetectionStrategy, Input } from '@angular/core';
import { PageFrameService } from './page-frame.service';

@Component({
  selector: 'app-page-frame',
  templateUrl: './page-frame.component.html',
  styleUrls: ['./page-frame.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PageFrameComponent {

  @Input() pageTitle: string = '';

  constructor(private readonly service: PageFrameService) { }

  onMenuButtonClicked(): void {
    this.service.navToggleRequested.next(undefined);
  }

}
