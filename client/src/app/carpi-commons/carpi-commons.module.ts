import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { MatToolbarModule } from '@angular/material/toolbar';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SimpleValueOutputComponent } from './simple-value-output/simple-value-output.component';
import { PageFrameComponent } from './page-frame/page-frame.component';
import { NavToggleButtonComponent } from './nav-toggle-button/nav-toggle-button.component';


@NgModule({
  declarations: [
    SimpleValueOutputComponent,
    PageFrameComponent,
    NavToggleButtonComponent
  ],
  imports: [
    CommonModule,

    MatToolbarModule,
    MatButtonModule,
    MatIconModule
  ],
  exports: [
    SimpleValueOutputComponent,
    PageFrameComponent,
    NavToggleButtonComponent
  ]
})
export class CarpiCommonsModule { }
