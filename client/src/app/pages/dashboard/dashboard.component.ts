import { Component, OnDestroy, OnInit } from '@angular/core';
import { ProgressBarMode } from '@angular/material/progress-bar';
import { map, Observable, of, startWith, Subscription } from 'rxjs';
import { BackendPollService } from 'src/app/backend/backend-poll.service';

@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.scss']
})
export class DashboardComponent implements OnInit, OnDestroy {

  subs: Subscription[] = []
  public currentSoC: number | undefined;
  public currentPowerOutput: number | undefined;
  public currentBatteryVoltage: number | undefined;
  public currentBatteryCurrent: number | undefined;

  constructor(
    public readonly backendPoller: BackendPollService
  ) { }

  private addSub(sub: Subscription): void {
    this.subs.push(sub);
  }

  ngOnInit(): void {
    // This is necessary, otherwise it wouldn't pull data at all for some reason
    this.addSub(this.backendPoller.subToField('carpi.gps.systimestamp')
      .pipe(startWith('-'))
      .subscribe(_ => {}));

    // Sub to the whole data set to extract certain value manually
    this.addSub(this.backendPoller.subToAll().subscribe(d => {
      // This makes it easier to display SoC bar
      this.currentSoC = d.getFloat('carpi.can.BatterySoC');

      this.currentBatteryVoltage = d.getFloat('carpi.can.BatteryVoltage');
      this.currentBatteryCurrent = d.getFloat('carpi.can.BatteryCurrent');
      this.setPowerOutput(this.currentBatteryVoltage, this.currentBatteryCurrent);
    }));
  }

  private setPowerOutput(v: number | undefined, a: number | undefined): void {
    if (DashboardComponent.hasValue(v) && DashboardComponent.hasValue(a)) {
      this.currentPowerOutput = v! * a! / -1000;
    } else {
      this.currentPowerOutput = undefined;
    }
  }

  ngOnDestroy(): void {
    this.subs.forEach(s => s.unsubscribe());
  }

  private static hasValue(n: number | undefined): boolean {
    return n !== undefined && !Number.isNaN(n);
  }

  public get batteryBarMode(): ProgressBarMode {
    if (this.hasPowerOutput && this.currentPowerOutput! < 0) {
      return 'buffer';
    } else {
      return 'determinate';
    }
  }

  public get batteryBarColor(): string {
    if (this.hasPowerOutput && this.currentPowerOutput! > 50) {
      return 'warn';
    } else {
      return 'primary';
    }
  }

  public get batteryChargeBarMode(): ProgressBarMode {
    if (this.hasPowerOutput && this.currentPowerOutput! > 0) {
      return 'buffer';
    } else {
      return 'determinate';
    }
  }

  public get hasSoC(): boolean {
    return DashboardComponent.hasValue(this.currentSoC);
  }

  public get hasPowerOutput(): boolean {
    return DashboardComponent.hasValue(this.currentPowerOutput);
  }

  getField(key: string): Observable<any> {
    return this.backendPoller.subToField(key);
  }

  get currentConsumption(): number {
    if (!DashboardComponent.hasValue(this.currentPowerOutput)) {
      return 0;
    }
    return Math.max(this.currentPowerOutput!, 0);
  }
  get currentChargingRate(): number {
    if (!DashboardComponent.hasValue(this.currentPowerOutput)) {
      return 0;
    }
    return Math.abs(Math.min(this.currentPowerOutput!, 0)) / 50 * 100;
  }

  get stateOfCharge(): Observable<number | undefined> {
    return this.backendPoller.subToFloat('carpi.can.BatterySoC');
  }

  get availableEnergy(): Observable<number | undefined> {
    return this.backendPoller.subToFloat('carpi.can.BatteryAvailableEnergy');
  }

  get batteryTemperature(): Observable<number | undefined> {
    return this.backendPoller.subToFloat('carpi.can.AvgBatteryTemp');
  }

  get isEcoModeEnabled(): Observable<boolean> {
    return this.backendPoller.subToFloat('carpi.can.EcoMode').pipe(
      map(v => v === 1)
    );
  }

  get gpsLatitude(): Observable<number | undefined> {
    return this.backendPoller.subToFloat('carpi.gps.latitude');
  }

  get gpsLongitude(): Observable<number | undefined> {
    return this.backendPoller.subToFloat('carpi.gps.longitude');
  }

  get gpsAltitude(): Observable<number | undefined> {
    return this.backendPoller.subToInt('carpi.gps.altitude');
  }

  get gpsAccuracy(): Observable<number | undefined> {
    return this.backendPoller.subToFloat('carpi.gps.epx');
  }
}
