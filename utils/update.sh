#!/bin/bash
set -e
ME=`basename "$0"`

# --- Parameters
P_URL="${1}"

# --- Settings
S_TEMP_FILE="${S_TEMP_FILE:=/tmp/carpi-webui.tmp}"
S_WEBDIR="${S_WEBDIR:=/var/www/html}"

# --- Validation
if [ -z "${P_URL}" ]; then
    echo "No URL provided! Run script like this:"
    echo " $ ./${ME} <LinkToArtifact>"
    exit 1
fi

# --- Cleanup
if [ -f "${S_TEMP_FILE}" ]; then
    echo "Deleting temp file ${S_TEMP_FILE} ..."
    rm "${S_TEMP_FILE}"
fi

# --- Download
echo "Downloading from ${P_URL} ..."
wget -4 -O "${S_TEMP_FILE}" "${P_URL}"

# --- Unpack
echo "Unpacking from archive to ${S_WEBDIR} ..."
tar -xzvf "${S_TEMP_FILE}" -C "${S_WEBDIR}"
