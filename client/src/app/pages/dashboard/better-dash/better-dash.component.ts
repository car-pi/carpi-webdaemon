import { Observable, map, zip, tap, filter, distinct, debounce, debounceTime, distinctUntilChanged, throttleTime, timer, interval, startWith, take } from 'rxjs';
import { Component, OnDestroy } from '@angular/core';
import { BackendPollService } from 'src/app/backend/backend-poll.service';
import { BackendService } from 'src/app/backend/backend.service';
import { DriveModeComponent } from '../drive-mode/drive-mode.component';
import { parseGpsFix, TpvObject } from 'src/app/utils/gps.utils';
import * as moment from 'moment';

@Component({
  templateUrl: './better-dash.component.html',
  styleUrls: ['./better-dash.component.scss']
})
export class BetterDashComponent extends DriveModeComponent {

  currentTime: string = '';

  constructor(
    backendPoller: BackendPollService,
    backend: BackendService
  ) {
    super(backendPoller, backend);
  }

  override ngOnInit(): void {
    super.ngOnInit();
    this.addSub(
      timer(0, 500).subscribe(() => {
        this.currentTime = `${moment().format('DD/MM/yyyy HH:mm:ss')}`;
      })
    );
  }

  get batteryPower(): Observable<number | undefined> {
    return this.backendPoller.subToFloat('custom.car.BatteryPower');
  }

  get gpsData(): Observable<TpvObject> {
    return this.backendPoller.subToString('carpi.gps.RAW').pipe(
      map((i) => {
        if (!i) {
          console.error('FAIL: Does not have a value');
          return null;
        }
        try {
          return JSON.parse(i);
        } catch (e) {
          console.error('FAIL', e);
          return null;
        }
      }),
      filter(i => !!i && i.class === 'TPV'),
    );
  }

  private getGpsData<T>(fn: (v: TpvObject) => T): Observable<T> {
    return this.gpsData.pipe(
      map(t => fn(t)),
      distinctUntilChanged(),
    );
  }

  get gpsSpeed(): Observable<number | undefined> {
    return this.backendPoller.subToFloat('carpi.gps.speed.kmh');
  }
  get gpsSpeedError(): Observable<number | undefined> {
    return this.backendPoller.subToFloat('carpi.gps.eps');
  }
  get gpsErrorX(): Observable<number | undefined> {
    return this.backendPoller.subToFloat('carpi.gps.epx');
  }
  get gpsErrorY(): Observable<number | undefined> {
    return this.backendPoller.subToFloat('carpi.gps.epy');
  }
  get gpsLatitude(): Observable<number | undefined> {
    return this.backendPoller.subToFloat('carpi.gps.latitude');
  }
  get gpsLongitude(): Observable<number | undefined> {
    return this.backendPoller.subToFloat('carpi.gps.longitude');
  }

  get gpsAccuracy(): Observable<number | undefined> {
    return zip(this.gpsErrorX, this.gpsErrorY).pipe(
      map(([x, y]) => Math.max(x || -1, y || -1)),
      map(a => a < 0 ? undefined : a)
    );
  }

  get gpsFixMode(): Observable<number | undefined> {
    return this.backendPoller.subToInt('carpi.gps.fixmode');
  }
  get gpsFixModeStr(): Observable<string> {
    return this.gpsFixMode.pipe(
      map(fixMode => parseGpsFix(fixMode))
    );
  }

  get gpsAltitude(): Observable<number | undefined> {
    return this.backendPoller.subToInt('carpi.gps.altitude');
  }

  get speedDiff(): Observable<number | undefined> {
    return zip(this.vehicleSpeed, this.gpsSpeed).pipe(
      map(([car, gps]) => {
        if (car !== undefined && gps !== undefined) {
          return car - gps;
        }
        return undefined;
      }),
    )
  }

  get speed(): Observable<number | undefined> {
    return zip(this.vehicleSpeed, this.gpsSpeed).pipe(
      map(([car, gps]) => {
        if (gps !== undefined) {
          if (car !== undefined && car < 1) {
            return 0
          }
          return gps;
        } else if (car !== undefined) {
          return car;
        } else {
          return 0;
        }
      })
    );
  }

  get time(): Observable<string> {
    return interval(500).pipe(
      map((_) => `${moment().format('HH:mm:ss')}`)
    );
  }

  get lastReportedTime(): Observable<string> {
    return this.backendPoller.subToString('carpi.reporter.last_submitted_ts').pipe(
      filter((i) => !!i),
      map((i) => `${moment(i).format('DD/MM/YYYY HH:mm:ss')}`),
      startWith('-')
    );
  }

  get lastFail(): Observable<string | undefined> {
    return this.backendPoller.subToString('carpi.reporter.last_failure');
  }

  get lastFailTime(): Observable<string> {
    return this.backendPoller.subToString('carpi.reporter.last_failure_ts').pipe(
      filter((i) => !!i),
      map((i) => `${moment(i).format('DD/MM/YYYY HH:mm:ss')}`),
      startWith('-')
    );
  }

  get showFailure(): Observable<boolean> {
    return zip(
      this.backendPoller.subToString('carpi.reporter.last_submitted_ts'),
      this.backendPoller.subToString('carpi.reporter.last_failure_ts')
    ).pipe(
      map(([lastSubmit, lastFail]) => {
        const lastSubmitTs: moment.Moment | null = lastSubmit ? moment(lastSubmit) : null;
        const lastFailTs: moment.Moment | null = lastFail ? moment(lastFail) : null;
        if (lastFailTs !== null) {
          return lastFailTs.isAfter(lastSubmitTs);
        }
        return false;
      })
    );
  }

  showFailureReason(): void {
    this.backendPoller.subToString('carpi.reporter.last_failure').pipe(
      take(1)
    ).subscribe((v) => {
      alert(v);
    });
  }

}
