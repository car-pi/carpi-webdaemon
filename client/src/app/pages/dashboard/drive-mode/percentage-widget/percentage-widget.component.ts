import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { SimpleWidgetComponent } from '../simple-widget/simple-widget.component';

@Component({
  selector: 'app-percentage-widget',
  templateUrl: './percentage-widget.component.html',
  styleUrls: ['./percentage-widget.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PercentageWidgetComponent extends SimpleWidgetComponent {

  @Input() maxValue: number = 100;

  @Input() absoluteValue: number | null | undefined;
  @Input() absoluteUnit: string | undefined;

  constructor() {
    super();
  }

  get progressBarValue(): number {
    return this.value / this.maxValue * 100;
  }

  get hasAbsoluteValue(): boolean {
    return this.absoluteValue !== null && this.absoluteValue !== undefined;
  }

}
