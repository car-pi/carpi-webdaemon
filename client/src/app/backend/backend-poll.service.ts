import { Injectable, OnDestroy } from '@angular/core';
import { catchError, filter, interval, map, mergeMap, Observable, of, startWith, Subscription, switchMap, tap } from 'rxjs';
import { BackendService } from './backend.service';
import { BackendData } from './entities';

@Injectable({
  providedIn: 'root'
})
export class BackendPollService implements OnDestroy {

  subs: Subscription[] = [];
  currentData?: BackendData;

  poller$: Observable<BackendData | null>;

  constructor(private readonly backend: BackendService) {
    this.poller$ = interval(250)
      .pipe(
        mergeMap(() => backend.data
          .pipe(
            catchError(() => of(null))
          )),
        // ensures that we always have some data by filtering out null
        filter(d => d !== null),
        tap(d => {
          // Hack to make the compiler happy
          if (d) {
            this.currentData = d;
          }
        })
      );
  }

  private addSub(sub: Subscription): void {
    this.subs.push(sub);
  }

  private commonValueSub(): Observable<BackendData | null | undefined> {
    return this.poller$.pipe(
      startWith(this.currentData),
      filter(d => d !== null && d !== undefined)
    );
  }

  public subToAll(): Observable<BackendData> {
    return this.commonValueSub().pipe(
      map(d => d!)
    );
  }

  public subToField(fieldName: string): Observable<any> {
    return this.commonValueSub().pipe(
      map(d => d!.getFieldValue(fieldName))
    );
  }

  public subToString(fieldName: string): Observable<string | undefined> {
    return this.commonValueSub().pipe(
      map(d => d!.getString(fieldName))
    );
  }

  public subToFloat(fieldName: string): Observable<number | undefined> {
    return this.commonValueSub().pipe(
      map(d => d!.getFloat(fieldName))
    );
  }

  public subToInt(fieldName: string): Observable<number | undefined> {
    return this.commonValueSub().pipe(
      map(d => d!.getInteger(fieldName))
    );
  }

  public subToBoolean(fieldName: string): Observable<boolean | undefined> {
    return this.commonValueSub().pipe(
      map(d => d!.getBoolean(fieldName))
    );
  }

  ngOnDestroy(): void {
    this.subs.forEach(s => {
      s.unsubscribe();
    });
  }
}
