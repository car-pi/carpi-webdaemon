import { filter, forkJoin, interval, map, timer, tap, mergeMap, catchError, of, takeWhile } from 'rxjs';
import { Component } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { MatBottomSheet } from '@angular/material/bottom-sheet';
import { MatSnackBar } from '@angular/material/snack-bar';
import { BackendService } from 'src/app/backend/backend.service';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { ServiceCommandDialogComponent, ServiceCommandDialogData } from './service-command-dialog/service-command-dialog.component';
import { ProcessingDialogComponent } from './processing-dialog/processing-dialog.component';


const PROTECTED_SERVICES = ['commanddaemon', 'webdaemon', 'dotnet-webdaemon'];


@Component({
  selector: 'app-command',
  templateUrl: './command.component.html',
  styleUrls: ['./command.component.scss']
})
export class CommandComponent {

  commandForm: FormGroup;

  servicesToUpdate: [string, string, string?][] = [
    ['commanddaemon', 'Command Daemon', 'keyboard_command_key'],
    ['dataloggerdaemon', 'Data Logger Daemon', 'text_snippet'],
    ['displaydaemon', 'Display Daemon', 'desktop_windows'],
    ['gpsdaemon', 'GPS Daemon', 'satellite_alt'],
    ['netdaemon', 'Network Daemon', 'settings_ethernet'],
    ['obddaemon', 'OBD Daemon', 'directions_car_filled'],
    ['dotnet-reporter', 'Reporter', 'satellite_alt'],
    //['webdaemon', 'Web Daemon', 'app_shortcut']
  ];
  servicesToRestart: [string, string, string?][] = [
    ['commanddaemon', 'Command Daemon', 'keyboard_command_key'],
    ['dataloggerdaemon', 'Data Logger Daemon', 'text_snippet'],
    ['dotnet-data-transformer', 'Data Transformer Daemon'],
    ['display', 'Display Daemon', 'desktop_windows'],
    ['gpsdaemon', 'GPS Daemon', 'satellite_alt'],
    ['netdaemon', 'Network Daemon', 'settings_ethernet'],
    ['obddaemon', 'OBD Daemon', 'directions_car_filled'],
    ['dotnet-reporter', 'Reporter', 'satellite_alt'],
    ['dotnet-webdaemon', 'Web Daemon', 'app_shortcut']
  ];

  constructor(
    fb: FormBuilder,
    private readonly backendService: BackendService,
    private readonly snackBar: MatSnackBar,
    private readonly bottomSheet: MatBottomSheet
  ) {
    this.commandForm = fb.group({
      commandName: ['', Validators.required]
    });
  }

  onSendCommandClicked(): void {
    if (!this.commandForm.valid) {
      this.snackBar.open('A command is required', undefined, { duration: 2500 });
      return;
    }
    const commandData: {
      commandName: string
    } = this.commandForm.value;

    this.sendCommand(commandData.commandName);
  }

  sendCommand(commandName: string, params?: any): void {
    this.backendService.postCommand(commandName, params).subscribe({
      complete: () => {
        this.snackBar.open('Command invoked', undefined, { duration: 2500 });
      },
      error: () => {
        this.snackBar.open('Failed to invoke command', undefined, { duration: 2500 });
      }
    })
  }

  updateService(serviceName: string): void {
    this.sendCommand('update.service', { service: serviceName })
  }

  restartService(service: [string, string, string?]): void {
    const context: ServiceCommandDialogData = {
      service: service,
      // don't let me kill commanddaemon LUL
      disableStop: PROTECTED_SERVICES.indexOf(service[0]) >= 0
    };
    this.bottomSheet.open(ServiceCommandDialogComponent, {
      data: context
    }).afterDismissed().subscribe(r => {
      if (!r) {
        return;
      }
      this.sendCommand(`sys.${r}_service`, { service: service[0] })
    });
  }

  shutdownSystem(): void {
    ConfirmDialogComponent.open(
      this.bottomSheet,
      'Shutdown CarPi?',
      'Do you want to shutdown CarPi?\n' +
      'After invoking this command, you will not be able to use CarPi ' +
      'until you restart the physical device.',
      'warning_amber',
      'warn'
    ).pipe(
      filter(i => i === true)
    ).subscribe(() => {
      this.sendCommand('sys.shutdown');
    });
  }

  rebootSystem(): void {
    ConfirmDialogComponent.open(
      this.bottomSheet,
      'Reboot CarPi?',
      'Do you want to reboot CarPi?\n' +
      'This may take up to 2 minutes.',
      'restart_alt'
    ).pipe(
      filter(i => i === true)
    ).subscribe(() => {
      this.sendCommand('sys.reboot');
      ProcessingDialogComponent.open(
        this.bottomSheet,
        timer(30_000, 5_000).pipe(
          mergeMap(() => this.backendService.healthCheck.pipe(
            catchError((e) => of({ alive: false }))
          )),
          takeWhile((h) => !h.alive),
        ),
        'Rebooting your CarPi ...',
        'This might take up to 2 minutes. This dialog will close ' +
        'automatically once your CarPi is back online.');
    });
  }

}
