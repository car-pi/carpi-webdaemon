import { Component, OnInit, OnDestroy } from '@angular/core';
import { ECharts, EChartsOption, graphic } from 'echarts';
import * as moment from 'moment';
import { debounceTime, Subscription } from 'rxjs';
import { BackendPollService } from 'src/app/backend/backend-poll.service';
import { BackendService } from 'src/app/backend/backend.service';
import { SignalrValuesService } from 'src/app/backend/signalr-values.service';

const MAX_TIME = 60000;

@Component({
  selector: 'app-history',
  templateUrl: './history.component.html',
  styleUrls: ['./history.component.scss']
})
export class HistoryComponent implements OnInit, OnDestroy {

  chartInstance: ECharts | undefined;
  chartOption: EChartsOption = {
    animation: false,
    //grid: {
    //  top: 10,
    //  left: 40,
    //  right: 10,
    //  bottom: 40
    //},
    xAxis: [
      {
        type: 'time',
        boundaryGap: false,
        splitNumber: 5
      }
    ],
    yAxis: [
      {
        type: 'value',
        name: 'Power (kW)',
        min: -50,
        max: 50,
        interval: 10,
        axisLabel: {
          formatter: '{value} kW'
        }
      },
      {
        type: 'value',
        name: 'SoC (%)',
        min: 0,
        max: 100,
        interval: 20,
        axisLabel: {
          formatter: '{value} %'
        }
      },
      //{
      //  type: 'value',
      //  name: 'Speed (kmh)',
      //  min: 0,
      //  max: 140,
      //  interval: 20,
      //  axisLabel: {
      //    formatter: '{value} km/h'
      //  }
      //},
    ],
    toolbox: {
      feature: {
        dataZoom: {
          yAxisIndex: 'none'
        },
      }
    },
    //tooltip: {
    //  trigger: 'axis',
    //  axisPointer: {
    //    type: 'cross',
    //    crossStyle: {
    //      color: '#999'
    //    }
    //  }
    //},
    dataZoom: [
      { type: 'inside', start: 0, end: 100 },
      { start: 0, end: 100 }
    ],
    series: [
    ],
  };
  powerOutput: [string, number][] = [];
  soc: [string, number][] = [];
  speed: [string, number][] = [];
  subs: Subscription[] = [];

  autoScroll: boolean = true;

  constructor(
    private readonly backendPoller: BackendPollService,
    private readonly backend: BackendService,
    private readonly signalr: SignalrValuesService
  ) { }

  private addSub(sub: Subscription): void {
    this.subs.push(sub);
  }

  ngOnInit(): void {
  }

  ngOnDestroy(): void {
    this.subs.forEach(s => s.unsubscribe());
  }

  onChartInit($event: ECharts): void {
    this.chartInstance = $event;
    this.addSub(this.signalr.subToFloat('custom.car.BatteryPower').pipe(
      debounceTime(300)
    ).subscribe((powerOutput) => {
      console.log(moment().toISOString(), 'Battery Power', powerOutput);
      if (powerOutput !== undefined) {
        this.addPowerOutput(powerOutput);
      }
    }));
    this.addSub(this.signalr.subToFloat('carpi.can.BatterySoC').subscribe(soc => {
      console.log(moment().toISOString(), 'Battery SoC', soc);
      if (soc !== undefined) {
        this.addSoC(soc);
      }
    }));
    this.addSub(this.signalr.subToFloat('carpi.can.VehicleSpeed').subscribe(spd => {
      console.log(moment().toISOString(), 'Speed', spd);
      if (spd !== undefined) {
        this.addSpeed(spd);
      }
    }));
  }

  private addPowerOutput(powerOutput: number): void {
    this.powerOutput.push([
      moment().toISOString(),
      powerOutput
    ]);
    //if (this.data.length >= MAX) {
    //  this.data = [...this.data.slice(this.data.length - MAX)]
    //}
    this.updateChart();
  }

  private addSoC(soc: number) {
    this.soc.push([
      moment().toISOString(),
      soc
    ]);
    this.updateChart();
  }

  private addSpeed(spd: number) {
    this.speed.push([
      moment().toISOString(),
      spd
    ]);
    this.updateChart();
  }

  private updateChart(): void {
    this.chartInstance?.setOption({
      series: [{
        name: 'Power (kW)',
        data: this.powerOutput,
        showSymbol: false,
        type: 'line',
        clip: true,
        itemStyle: {
          color: 'rgb(255, 70, 131)'
        },
        areaStyle: {
          color: new graphic.LinearGradient(0, 0, 0, 1, [
            {
              color: 'rgb(255, 158, 68)',
              offset: 0
            },
            {
              color: 'rgb(255, 70, 131)',
              offset: 1
            }
          ])
        }
      }, {
        name: 'SoC (%)',
        yAxisIndex: 1,
        data: this.soc,
        showSymbol: false,
        type: 'line',
        clip: true,
        itemStyle: {
          color: 'rgb(0, 255, 131)'
        },
      //}, {
      //  name: 'Speed (kmh)',
      //  yAxisIndex: 2,
      //  data: this.speed,
      //  showSymbol: false,
      //  type: 'line',
      //  clip: true,
      //  itemStyle: {
      //    color: 'rgb(0, 255, 131)'
      //  },
      }]
    });
    if (this.autoScroll) {
      const combined = [...this.powerOutput, ...this.soc, ...this.speed]
        .map(i => i[0])
        .sort();
      const pastIdx = combined.filter(i => moment().diff(i) > MAX_TIME).sort();
      const limitValue = pastIdx.length === 0 ? null : pastIdx[pastIdx.length - 1];
      const startValue = combined[0];
      const endValue = combined[combined.length - 1];

      this.chartInstance?.setOption({
        dataZoom: [
          { type: 'inside', startValue: (limitValue || startValue), endValue, filterMode: 'none' },
        ],
      });
    }
  }

  toggleAutoScroll(): void {
    this.autoScroll = !this.autoScroll;
  }

}
