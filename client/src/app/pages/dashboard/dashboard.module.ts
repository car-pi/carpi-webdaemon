import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';

import { MatCardModule } from '@angular/material/card';
import { MatIconModule } from '@angular/material/icon';
import { MatProgressBarModule } from '@angular/material/progress-bar';
import { MatProgressSpinnerModule } from '@angular/material/progress-spinner';
import { MatListModule } from '@angular/material/list';
import { MatButtonModule } from '@angular/material/button';

import { CarpiCommonsModule } from '../../carpi-commons/carpi-commons.module';

import { DashboardRoutingModule } from './dashboard-routing.module';
import { DashboardComponent } from './dashboard.component';
import { DriveModeComponent } from './drive-mode/drive-mode.component';
import { PowerConsumptionWidgetComponent } from './drive-mode/power-consumption-widget/power-consumption-widget.component';
import { SimpleWidgetComponent } from './drive-mode/simple-widget/simple-widget.component';
import { BooleanWidgetComponent } from './drive-mode/boolean-widget/boolean-widget.component';
import { PercentageWidgetComponent } from './drive-mode/percentage-widget/percentage-widget.component';
import { NgxEchartsModule } from 'ngx-echarts';
import { CarDashComponent } from './car-dash/car-dash.component';
import { BetterDashComponent } from './better-dash/better-dash.component';


@NgModule({
  declarations: [
    DashboardComponent,
    DriveModeComponent,
    PowerConsumptionWidgetComponent,
    SimpleWidgetComponent,
    BooleanWidgetComponent,
    PercentageWidgetComponent,
    CarDashComponent,
    BetterDashComponent
  ],
  imports: [
    CommonModule,
    HttpClientModule,

    DashboardRoutingModule,
    CarpiCommonsModule,

    MatCardModule,
    MatIconModule,
    MatProgressBarModule,
    MatProgressSpinnerModule,
    MatListModule,
    MatButtonModule,

    NgxEchartsModule
  ]
})
export class DashboardModule { }
