import { Observable } from 'rxjs';
import { Component, Inject, OnInit } from '@angular/core';
import { MatBottomSheet, MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';

@Component({
  templateUrl: './processing-dialog.component.html',
  styleUrls: ['./processing-dialog.component.scss']
})
export class ProcessingDialogComponent implements OnInit {

  constructor(
    private readonly dialogRef: MatBottomSheetRef<ProcessingDialogComponent>,
    @Inject(MAT_BOTTOM_SHEET_DATA) public readonly data: {
      observable: Observable<any>;
      title: string;
      message: string;
    }
  ) { }

  ngOnInit(): void {
    this.data.observable.subscribe({
      complete: () => {
        this.dialogRef.dismiss();
      }
    });
  }

  static open(
    bottomSheet: MatBottomSheet,
    observable: Observable<any>,
    title: string,
    message: string,
  ): MatBottomSheetRef<ProcessingDialogComponent> {
    return bottomSheet.open(ProcessingDialogComponent, {
      data: {
        observable,
        title,
        message
      },
      disableClose: true
    });
  }

}
