import { Component, OnDestroy, OnInit } from '@angular/core';
import { interval, map, Observable, Subscription, mergeMap, catchError, of, timer } from 'rxjs';
import { BackendPollService } from 'src/app/backend/backend-poll.service';
import { BackendService } from 'src/app/backend/backend.service';
import { hasNumberValue } from 'src/app/utils/number.utils';

@Component({
  templateUrl: './drive-mode.component.html',
  styleUrls: ['./drive-mode.component.scss']
})
export class DriveModeComponent implements OnInit, OnDestroy {

  subs: Subscription[] = [];
  currentPowerOutput: number | undefined;

  isBackendAlive: boolean = false;

  constructor(
    protected readonly backendPoller: BackendPollService,
    protected readonly backend: BackendService
  ) { }

  protected addSub(sub: Subscription): void {
    this.subs.push(sub);
  }

  ngOnInit(): void {
    this.addSub(this.backendPoller.subToAll().subscribe(d => {
      this.setPowerOutput(
        d.getFloat('carpi.can.BatteryVoltage'),
        d.getFloat('carpi.can.BatteryCurrent')
      );
    }));
    this.addSub(timer(0, 5000).pipe(
      mergeMap(() => this.backend.healthCheck.pipe(
        catchError(() => of(null))
      ))
    ).subscribe({
      next: r => {
        this.isBackendAlive = r?.alive || false;
      },
      error: e => {
        this.isBackendAlive = false;
      }
    }))
  }

  ngOnDestroy(): void {
    this.subs.forEach(s => s.unsubscribe());
  }

  protected setPowerOutput(v: number | undefined, a: number | undefined): void {
    if (hasNumberValue(v) && hasNumberValue(a)) {
      this.currentPowerOutput = v! * a! / -1000;
    } else {
      this.currentPowerOutput = undefined;
    }
  }

  get batteryTemperature(): Observable<number | undefined> {
    return this.backendPoller.subToFloat('carpi.can.AvgBatteryTemp');
  }
  get batterySoC(): Observable<number | undefined> {
    return this.backendPoller.subToFloat('carpi.can.BatterySoC');
  }
  get availableEnergy(): Observable<number | undefined> {
    return this.backendPoller.subToFloat('carpi.can.BatteryAvailableEnergy');
  }
  get isEcoModeEnabled(): Observable<boolean | undefined> {
    return this.backendPoller.subToFloat('carpi.can.EcoMode').pipe(
      map(v => {
        if (hasNumberValue(v)) {
          return v === 1;
        } else {
          return undefined;
        }
      })
    );
  }

  get availableChargingPower(): Observable<number | undefined> {
    return this.backendPoller.subToFloat('carpi.can.MaxGeneratedPower');
  }
  get availableDischargePower(): Observable<number | undefined> {
    return this.backendPoller.subToFloat('carpi.can.MaxAvailablePower');
  }

  get cabinTemperature(): Observable<number | undefined> {
    return this.backendPoller.subToFloat('carpi.can.InCarTemperature');
  }
  get exteriorTemperature(): Observable<number | undefined> {
    return this.backendPoller.subToFloat('carpi.can.ExternalTemperature');
  }

  get vehicleSpeed(): Observable<number | undefined> {
    return this.backendPoller.subToFloat('carpi.can.VehicleSpeed');
  }

  get vehicleState(): Observable<string | undefined> {
    return this.backendPoller.subToString('carpi.can.VehicleState.parsed');
  }

  get auxBatteryVoltage(): Observable<number | undefined> {
    return this.backendPoller.subToFloat('carpi.can.obd_voltage');
  }

}
