import { Component, OnInit, ChangeDetectionStrategy, OnDestroy, Input, Output, EventEmitter } from '@angular/core';
import { Subscription } from 'rxjs';
import { BackendPollService } from 'src/app/backend/backend-poll.service';

@Component({
  selector: 'app-simple-value-output',
  templateUrl: './simple-value-output.component.html',
  styleUrls: ['./simple-value-output.component.scss'],
  changeDetection: ChangeDetectionStrategy.Default
})
export class SimpleValueOutputComponent implements OnInit, OnDestroy {

  currentValue: any;

  private valueSub$: Subscription | undefined;
  private _key: string | undefined = undefined;

  @Output() valueChanged = new EventEmitter<any>();

  constructor(private readonly backendPoller: BackendPollService) { }

  @Input() get key(): string | undefined {
    return this._key;
  }
  set key(key: string | undefined) {
    this._key = key;
    this.updateSub();
  }

  private updateSub() {
    this.unsub();
    if (this.key) {
      this.valueSub$ = this.backendPoller.subToField(this.key)
        .subscribe(v => {
          this.currentValue = v;
          this.valueChanged.emit(v);
        });
    }
  }

  private unsub(): void {
    if (this.valueSub$) {
      this.valueSub$.unsubscribe();
    }
  }

  ngOnInit(): void {
    this.updateSub();
  }

  ngOnDestroy(): void {
    this.unsub();
  }

}
