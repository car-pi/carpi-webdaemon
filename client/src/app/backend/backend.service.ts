import { BackendData, IBackendData, IHealthCheck } from './entities';
import { HttpClient } from '@angular/common/http';
import { Inject, Injectable, InjectionToken } from '@angular/core';
import { map, Observable, of, retry } from 'rxjs';

export const BACKEND_URL = new InjectionToken<string>('BackendUrl');

@Injectable({
  providedIn: 'root'
})
export class BackendService {
  constructor(
    private readonly http: HttpClient,
    @Inject(BACKEND_URL) private readonly baseUrl: string
  ) { }

  private getUrl(route: string): string {
    return `${this.baseUrl}/${route}`;
  }

  get serverUrl(): string {
    return this.baseUrl;
  }

  get data(): Observable<BackendData> {
    return this.http.get<IBackendData>(
      this.getUrl(`?t=${new Date().getTime()}`)
    ).pipe(
      map(d => new BackendData(d))
    );
  }

  get healthCheck(): Observable<IHealthCheck> {
    return this.http.get<IHealthCheck>(
      this.getUrl('health')
    );
  }

  postCommand(command: string, params?: any): Observable<void> {
    return this.http.post<void>(
      this.getUrl(`command/${command}`),
      params || {},
      {
        headers: {
          'Content-Type': 'application/json'
        }
      }
    );
  }
}
