import { ECharts, EChartsOption } from 'echarts';
import { Component, OnDestroy, OnInit } from '@angular/core';
import { BackendPollService } from 'src/app/backend/backend-poll.service';
import { Subscription } from 'rxjs';
import { formatNumber, hasNumberValue } from 'src/app/utils/number.utils';

@Component({
  templateUrl: './car-dash.component.html',
  styleUrls: ['./car-dash.component.scss']
})
export class CarDashComponent implements OnInit, OnDestroy {

  private chartInstance: ECharts | undefined;
  private subs: Subscription[] = [];

  chartOption: EChartsOption = {
    series: [
      {
        type: 'gauge',
        name: 'Charge Power',
        data: [],
        center: ['25%', '69%'],
        min: -50,
        max: 0,
        splitNumber: 5,
        //startAngle: -90,
        endAngle: 180,
        detail: {
          show: false,
          color: 'auto',
        },
        axisLabel: {
          formatter: (value) => {
            return value % 20 === 0 ? `${value}` : '';
          }
        },
        axisLine: {
          lineStyle: {
            width: 6,
            color: [
              [1, '#58D9F9'],
            ]
          }
        },
        axisTick: {
          splitNumber: 2
        },
        pointer: {
          itemStyle: {
            color: 'auto'
          }
        },
        animationEasing: 'linear',
        animationEasingUpdate: 'linear',
      },
      {
        type: 'gauge',
        name: 'Power',
        data: [15],
        center: ['25%', '69%'],
        min: 0,
        max: 100,
        splitNumber: 10,
        startAngle: 180,
        //endAngle: 90,
        detail: {
          formatter: (p) => `${formatNumber(p)}\nkW`,
          color: 'auto',
        },
        axisLine: {
          lineStyle: {
            width: 6,
            color: [
              [-0.001, '#58D9F9'],
              [0.50, '#7CFFB2'],
              [0.90, '#FDDD60'],
              [1.0, '#FF6E76'],
            ]
          }
        },
        axisTick: {
          splitNumber: 4
        },
        pointer: {
          itemStyle: {
            color: 'auto'
          }
        },
        animationEasing: 'linear',
        animationEasingUpdate: 'linear',
      },
      {
        type: 'gauge',
        name: 'Speed',
        data: [],
        center: ['75%', '69%'],
        min: 0,
        max: 140,
        splitNumber: 14,
        detail: {
          formatter: (p) => `${formatNumber(p, 0)}`,
          color: 'auto',
        },
        axisLine: {
          lineStyle: {
            width: 6,
            color: [
              [100/140, '#7CFFB2'],
              [135/140, '#FDDD60'],
              [1, '#FF6E76'],
            ]
          }
        },
        axisLabel: {
          formatter: (value) => {
            return value % 20 === 0 ? `${value}` : '';
          }
        },
        axisTick: {
          splitNumber: 4
        },
        pointer: {
          itemStyle: {
            color: 'auto'
          }
        },
      },
      // Top Row
      {
        type: 'gauge',
        name: 'Battery Temperature',
        data: [],
        center: ['40%', '35%'],
        radius: '60%',
        min: -10,
        max: 50,
        splitNumber: 6,
        startAngle: 135,
        endAngle: 60,
        detail: {
          formatter: (p) => `${formatNumber(p, 1)}°c`,
          offsetCenter: ['-70%', '-90%'],
          color: 'auto',
          fontSize: 15,
          lineHeight: 15,
        },
        axisLine: {
          lineStyle: {
            width: 6,
            color: [
              [0.25, '#58D9F9'],
              [0.71, '#7CFFB2'],
              [0.90, '#FDDD60'],
              [1.0, '#FF6E76'],
            ]
          }
        },
        axisLabel: {
          formatter: (value) => {
            if (Math.abs(value) / 10 % 2 === 0) {
              return `${value}°`;
            }
            return ``;
          }
        },
        axisTick: {
          splitNumber: 2
        },
        pointer: {
          itemStyle: {
            color: 'auto'
          }
        },
      },
      {
        type: 'gauge',
        name: 'Battery SoC',
        data: [],
        center: ['60%', '35%'],
        radius: '60%',
        min: 0,
        max: 100,
        splitNumber: 4,
        startAngle: 120,
        endAngle: 45,
        clockwise: true,
        detail: {
          formatter: (p) => `${formatNumber(p, 1)}%`,
          offsetCenter: ['70%', '-90%'],
          color: 'auto',
          fontSize: 15,
          lineHeight: 15,
        },
        axisLine: {
          lineStyle: {
            width: 6,
            color: [
              [0.07, '#FF6E76'],
              [0.25, '#FDDD60'],
              [0.75, '#7CFFB2'],
              [0.9, '#58D9F9'],
            ]
          }
        },
        axisLabel: {
          formatter: (value) => {
            switch (value) {
              case 0: return '0';
              case 50: return '1/2';
              case 100: return '1';
              default: return '';
            }
          },
        },
        axisTick: {
          splitNumber: 2
        },
        pointer: {
          itemStyle: {
            color: 'auto'
          }
        },
      },
      {
        type: 'gauge',
        name: 'Available Energy',
        data: [50],
        center: ['85%', '25%'],
        radius: '40%',
        min: 0,
        max: 50,
        splitNumber: 4,
        startAngle: 120,
        endAngle: 45,
        clockwise: true,
        detail: {
          formatter: (p) => `${formatNumber(p, 2)}\nkWh`,
          offsetCenter: ['65%', '0%'],
          color: 'auto',
          fontSize: 15,
          lineHeight: 15
        },
        axisLine: {
          lineStyle: {
            width: 6,
            color: [
              [0.07, '#FF6E76'],
              [0.25, '#FDDD60'],
              [0.75, '#7CFFB2'],
              [0.9, '#58D9F9'],
            ]
          }
        },
        axisLabel: {
          formatter: (value) => '',
        },
        axisTick: {
          splitNumber: 2
        },
        pointer: {
          itemStyle: {
            color: 'auto'
          }
        },
      }
    ]
  };

  constructor(
    private readonly backendPoller: BackendPollService
  ) { }

  private addSub(sub: Subscription): void {
    this.subs.push(sub);
  }

  private setValues(
    power: number | undefined,
    vehicleSpeed: number,
    batteryPct: number,
    batteryTemp: number,
    batteryAvailableEnergy: number,
    maxGeneratedPower: number,
    maxAvailablePower: number
  ): void {
    const patch: EChartsOption = {
      series: [
        { data: [power] },
        { data: [power] },
        { data: [vehicleSpeed] },
        { data: [batteryTemp] },
        { data: [batteryPct] },
        { data: [batteryAvailableEnergy] }
      ]
    };
    this.chartInstance?.setOption(patch)
  }

  ngOnInit(): void {
    this.addSub(
      this.backendPoller.subToAll().subscribe(d => {
        const power = this.getPowerOutput(
          d.getFloat('carpi.can.BatteryVoltage'),
          d.getFloat('carpi.can.BatteryCurrent')
        );
        const bat = d.getFloat('carpi.can.BatterySoC');
        const spd = d.getFloat('carpi.can.VehicleSpeed');

        const tempBat = d.getFloat('carpi.can.AvgBatteryTemp');
        const tempInt = d.getFloat('carpi.can.InCarTemperature');
        const tempExt = d.getFloat('carpi.can.ExternalTemperature');

        const batEnergy = d.getFloat('carpi.can.BatteryAvailableEnergy');

        const maxGenPower = d.getFloat('carpi.can.MaxGeneratedPower');
        const maxAvailablePower = d.getFloat('carpi.can.MaxAvailablePower');

        this.setValues(
          power,
          spd,
          bat,
          tempBat,
          batEnergy,
          maxGenPower * -1,
          maxAvailablePower
        );
      })
    );
  }

  ngOnDestroy(): void {
    this.subs.forEach(s => s.unsubscribe());
  }

  onChartInit($event: ECharts): void {
    this.chartInstance = $event;
  }

  private getPowerOutput(v: number | undefined, a: number | undefined): number | undefined {
    if (hasNumberValue(v) && hasNumberValue(a)) {
      return v! * a! / -1000;
    } else {
      return undefined;
    }
  }

}
