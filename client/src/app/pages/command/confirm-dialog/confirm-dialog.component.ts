import { Component, Inject, OnInit } from '@angular/core';
import { MatBottomSheet, MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';
import { ThemePalette } from '@angular/material/core';
import { Observable } from 'rxjs';

@Component({
  templateUrl: './confirm-dialog.component.html',
  styleUrls: ['./confirm-dialog.component.scss']
})
export class ConfirmDialogComponent implements OnInit {

  constructor(
    @Inject(MAT_BOTTOM_SHEET_DATA) public readonly data: {
      title: string;
      message: string;
      icon?: string;
      color?: ThemePalette;
    },
    private readonly dialog: MatBottomSheetRef<ConfirmDialogComponent>
  ) { }

  ngOnInit(): void {
  }

  confirm(): void {
    this.dialog.dismiss(true);
  }
  cancel(): void {
    this.dialog.dismiss(false);
  }

  static open(
    bottomSheet: MatBottomSheet,
    title: string,
    message: string,
    icon?: string,
    color?: ThemePalette
  ): Observable<boolean | undefined> {
    return bottomSheet.open(ConfirmDialogComponent, {
      data: {
        title,
        message,
        icon,
        color,
      }
    }).afterDismissed();
  }

}
