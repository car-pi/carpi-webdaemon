import { Component, Inject } from '@angular/core';
import { MatBottomSheetRef, MAT_BOTTOM_SHEET_DATA } from '@angular/material/bottom-sheet';

export interface ServiceCommandDialogData {
  service: [string, string, string?];
  disableStop?: boolean;
}

@Component({
  templateUrl: './service-command-dialog.component.html',
  styleUrls: ['./service-command-dialog.component.scss']
})
export class ServiceCommandDialogComponent {

  serviceName: string;
  serviceIcon: string;

  constructor(
    private readonly sheet: MatBottomSheetRef<ServiceCommandDialogComponent>,
    @Inject(MAT_BOTTOM_SHEET_DATA) private readonly data: ServiceCommandDialogData
  ) {
    this.serviceName = data.service[1];
    this.serviceIcon = data.service[2] || 'miscellaneous_services';
  }

  get isStopDisabled(): boolean {
    return !!this.data.disableStop;
  }

  closeDialog(command?: string) {
    this.sheet.dismiss(command);
  }

}
