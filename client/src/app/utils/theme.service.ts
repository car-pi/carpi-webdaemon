import { BreakpointObserver } from '@angular/cdk/layout';
import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

const DARK_THEME_SELECTOR = '(prefers-color-scheme: dark)';


@Injectable({
  providedIn: 'root'
})
export class ThemeService {

  private isThemeDark: boolean;
  private themeChangeSub: Subject<boolean> = new Subject<boolean>();

  constructor(
    private readonly observer: BreakpointObserver
  ) {
    this.isThemeDark = window.matchMedia && window.matchMedia(DARK_THEME_SELECTOR).matches;

    this.observer.observe([DARK_THEME_SELECTOR]).subscribe(r => {
      const isDark = r.matches;
      this.isThemeDark = isDark;
      this.themeChangeSub.next(isDark);
    });
    this.themeChangeSub.subscribe(i => this.updateTheme());

    const storedPreference = this.readDarkModeFromLocalStorage();
    if (storedPreference !== undefined) {
      this.isDarkModeOn = storedPreference;
    }
  }

  get whenThemeChanges(): Subject<boolean> {
    return this.themeChangeSub;
  }

  get isDarkModeOn(): boolean {
    return this.isThemeDark;
  }
  set isDarkModeOn(enabled: boolean) {
    this.isThemeDark = enabled;
    this.writeDarkModeToLocalStorage(enabled);
    this.themeChangeSub.next(enabled);
  }

  private readDarkModeFromLocalStorage(): boolean | undefined {
    const value = localStorage.getItem('DarkModeEnabled');
    if (value !== null) {
      return value === '1';
    }
    return undefined;
  }
  private writeDarkModeToLocalStorage(preference: boolean | undefined) {
    if (preference === undefined) {
      localStorage.removeItem('DarkModeEnabled');
    } else {
      localStorage.setItem('DarkModeEnabled', preference ? '1' : '0');
    }
  }

  private updateTheme(): void {
    document.body.setAttribute('data-theme', this.isThemeDark ? 'dark' : 'light');
  }
}
