import { Component, ChangeDetectionStrategy, Input } from '@angular/core';

@Component({
  selector: 'app-boolean-widget',
  templateUrl: './boolean-widget.component.html',
  styleUrls: ['./boolean-widget.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class BooleanWidgetComponent {

  @Input() label: string | undefined;
  @Input() value: boolean | null | undefined;

  @Input() displayMode: 'text' | 'icon' | 'both' = 'text';

  @Input() onLabel: string = 'On';
  @Input() onIcon: string = 'check';
  @Input() offLabel: string = 'Off';
  @Input() offIcon: string = 'cross';
  @Input() noValueLabel: string = 'n/v';
  @Input() noValueIcon: string = '';

  constructor() { }

  get showText(): boolean {
    return this.displayMode === 'text' || this.displayMode === 'both';
  }
  get showIcon(): boolean {
    return this.displayMode === 'icon' || this.displayMode === 'both';
  }

  get isEnabled(): boolean {
    return this.value === true;
  }
  get isDisabled(): boolean {
    return this.value === false;
  }
  get hasValue(): boolean {
    return this.value !== null && this.value !== undefined;
  }

}
