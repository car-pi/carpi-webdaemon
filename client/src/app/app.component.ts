import { ThemeService } from './utils/theme.service';
import { AfterViewInit, Component, ViewChild } from '@angular/core';
import { BreakpointObserver } from '@angular/cdk/layout';
import { MatSlideToggleChange } from '@angular/material/slide-toggle';
import { MatSidenav } from '@angular/material/sidenav';
import { PageFrameService } from './carpi-commons/page-frame/page-frame.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements AfterViewInit {
  @ViewChild(MatSidenav)
  sidenav!: MatSidenav;

  isThemeDark = true;
  isMobile = true;

  constructor(
    private readonly themeService: ThemeService,
    private readonly observer: BreakpointObserver,
    private readonly pageFrameService: PageFrameService
  ) {
    this.isThemeDark = this.themeService.isDarkModeOn;
  }

  ngAfterViewInit(): void {
    setTimeout(() => {
      this.observer.observe(['(max-width: 800px)']).subscribe(r => {
        this.isMobile = r.matches;
        if (r.matches) {
          this.sidenav.mode = 'over';
          this.sidenav.close();
        } else {
          this.sidenav.mode = 'side';
          this.sidenav.open();
        }
      });

      this.pageFrameService.navToggleRequested.subscribe(n => {
        if (n !== undefined) {
          this.sidenav.opened = n;
        } else {
          this.sidenav.toggle();
        }
      });
    });
  }

  get themeIcon(): string {
    return this.isThemeDark ? 'dark_mode': 'light_mode';
  }

  onLightDarkModeToggled(toggle: MatSlideToggleChange): void {
    this.themeService.isDarkModeOn = this.isThemeDark;
  }

  onNavLinkClicked(): void {
    if (this.isMobile) {
      this.sidenav.close();
    }
  }

  onForceReloadClicked(): void {
    window.location.reload();
  }
}
