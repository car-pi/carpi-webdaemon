import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { NgxEchartsModule } from 'ngx-echarts';

import { HistoryRoutingModule } from './history-routing.module';
import { HistoryComponent } from './history.component';
import { CarpiCommonsModule } from 'src/app/carpi-commons/carpi-commons.module';
import { MatIconModule } from '@angular/material/icon';
import { MatButtonModule } from '@angular/material/button';
import { SignalrValuesService } from 'src/app/backend/signalr-values.service';


@NgModule({
  declarations: [
    HistoryComponent
  ],
  imports: [
    CommonModule,
    HistoryRoutingModule,
    MatIconModule,
    MatButtonModule,

    NgxEchartsModule,

    CarpiCommonsModule
  ],
  providers: [
    SignalrValuesService
  ]
})
export class HistoryModule { }
