import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { HttpClientModule } from '@angular/common/http';
import { MatButtonModule } from '@angular/material/button';
import { MatFormFieldModule } from '@angular/material/form-field';
import { MatInputModule } from '@angular/material/input';
import { MatListModule } from '@angular/material/list';
import { MatIconModule } from '@angular/material/icon';
import { MatSnackBarModule } from '@angular/material/snack-bar';
import { MatExpansionModule } from '@angular/material/expansion';
import { MatBottomSheetModule } from '@angular/material/bottom-sheet';

import { CommandRoutingModule } from './command-routing.module';
import { CommandComponent } from './command.component';
import { ServiceCommandDialogComponent } from './service-command-dialog/service-command-dialog.component';
import { CarpiCommonsModule } from 'src/app/carpi-commons/carpi-commons.module';
import { ConfirmDialogComponent } from './confirm-dialog/confirm-dialog.component';
import { ProcessingDialogComponent } from './processing-dialog/processing-dialog.component';
import { MatProgressBarModule } from '@angular/material/progress-bar';


@NgModule({
  declarations: [
    CommandComponent,
    ServiceCommandDialogComponent,
    ConfirmDialogComponent,
    ProcessingDialogComponent
  ],
  imports: [
    CommonModule,
    CommandRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,

    CarpiCommonsModule,

    MatButtonModule,
    MatInputModule,
    MatFormFieldModule,
    MatListModule,
    MatIconModule,
    MatSnackBarModule,
    MatExpansionModule,
    MatBottomSheetModule,
    MatProgressBarModule,
  ]
})
export class CommandModule { }
