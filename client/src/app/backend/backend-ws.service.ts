import { Injectable, OnDestroy } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class BackendWsService implements OnDestroy {

  constructor() { }

  ngOnDestroy(): void {
  }
}
