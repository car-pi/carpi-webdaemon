import { Injectable } from '@angular/core';
import { Subject } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class PageFrameService {

  private readonly navToggleRequestedSubject = new Subject<boolean | undefined>();

  constructor() { }

  get navToggleRequested(): Subject<boolean | undefined> {
    return this.navToggleRequestedSubject;
  }
}
