import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BetterDashComponent } from './better-dash/better-dash.component';
import { CarDashComponent } from './car-dash/car-dash.component';
import { DashboardComponent } from './dashboard.component';
import { DriveModeComponent } from './drive-mode/drive-mode.component';

const routes: Routes = [
  { path: '', component: DashboardComponent },
  { path: 'better', component: BetterDashComponent },
  { path: 'drive', component: DriveModeComponent },
  { path: 'car', component: CarDashComponent }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class DashboardRoutingModule { }
