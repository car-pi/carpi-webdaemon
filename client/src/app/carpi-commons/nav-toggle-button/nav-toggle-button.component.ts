import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { PageFrameService } from '../page-frame/page-frame.service';

@Component({
  selector: 'app-nav-toggle-button',
  templateUrl: './nav-toggle-button.component.html',
  styleUrls: ['./nav-toggle-button.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class NavToggleButtonComponent {

  @Input() size: 'full'|'mini' = 'full';

  constructor(private readonly service: PageFrameService) { }

  onNavButtonClicked(): void {
    this.service.navToggleRequested.next(undefined);
  }

}
