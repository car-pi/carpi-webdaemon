export function hasNumberValue(num: number | null | undefined): boolean {
  return num !== null && num !== undefined && !Number.isNaN(num);
}

export function formatNumber(
  num: number | null | undefined,
  fractionDigits: number | undefined = 1,
  defaultValue: string = '---'
): string {
  if (num === null || num === undefined || Number.isNaN(num)) {
    return defaultValue;
  } else if (fractionDigits !== undefined) {
    return `${num.toFixed(fractionDigits)}`;
  } else {
    return `${num}`;
  }
}
