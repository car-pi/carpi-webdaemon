import { NgxEchartsModule } from 'ngx-echarts';
import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { FormsModule } from '@angular/forms';
import { HttpClientModule } from '@angular/common/http';

import { MatIconModule } from '@angular/material/icon';
import { MatSidenavModule } from '@angular/material/sidenav';
import { MatSlideToggleModule } from '@angular/material/slide-toggle';
import { MatListModule } from '@angular/material/list';
import { MatToolbarModule } from '@angular/material/toolbar';
import { MatButtonModule } from '@angular/material/button';
import { MatDividerModule } from '@angular/material/divider';

import { environment } from 'src/environments/environment';

import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';

import { CarpiCommonsModule } from './carpi-commons/carpi-commons.module';

import { BACKEND_URL } from './backend/backend.service';
import { HomePageComponent } from './pages/home-page/home-page.component';
import { SignalrValuesService } from './backend/signalr-values.service';

@NgModule({
  declarations: [
    AppComponent,
    HomePageComponent
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    FormsModule,
    HttpClientModule,

    MatIconModule,
    MatSidenavModule,
    MatSlideToggleModule,
    MatListModule,
    MatToolbarModule,
    MatButtonModule,
    MatDividerModule,

    NgxEchartsModule.forRoot({
      echarts: () => import('echarts')
    }),

    CarpiCommonsModule
  ],
  providers: [
    {
      provide: BACKEND_URL,
      useValue: environment.backendUrl
    },
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
