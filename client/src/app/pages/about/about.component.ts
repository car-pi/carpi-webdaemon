import { AfterContentInit, Component, OnDestroy, OnInit } from '@angular/core';
import { MatSnackBar } from '@angular/material/snack-bar';
import { catchError, interval, map, mergeMap, of, Subscription } from 'rxjs';
import { BackendService } from 'src/app/backend/backend.service';
import { BackendData, DataType } from 'src/app/backend/entities';

@Component({
  selector: 'app-about',
  templateUrl: './about.component.html',
  styleUrls: ['./about.component.scss']
})
export class AboutComponent implements AfterContentInit, OnDestroy {

  healthCheckIcon = 'pending';
  data: BackendData | null = null;

  subs: Subscription[] = [];

  constructor(
    private readonly backendService: BackendService,
    private readonly snackBar: MatSnackBar
  ) { }

  ngAfterContentInit(): void {
    this.connect();
  }

  ngOnDestroy(): void {
    this.disconnect();
  }

  disconnect(): void {
    for (const sub of this.subs) {
      sub.unsubscribe();
    }
  }

  reconnect(): void {
    const ref = this.snackBar.open('Reconnecting ...');
    this.disconnect();
    setTimeout(() => {
      ref.dismiss();
      this.connect();
    }, 2000);
  }

  connect(): void {
    this.backendService.healthCheck.pipe(
      map(r => {
        return r.alive;
      }),
      catchError(e => {
        return of(false);
      })
    ).subscribe({
      next: r => {
        this.setHealthCheckIcon(r ? 'check' : 'warning');
        this.snackBar.open('Connected!', undefined, { duration: 2500 });
        const dataLoadSub = interval(1000)
          .pipe(
            mergeMap(() => this.backendService.data),
            catchError(() => of(undefined))
          )
          .subscribe({
            next: d => {
              if (d !== undefined) {
                this.data = d;
                this.setHealthCheckIcon('check');
              } else {
                this.setHealthCheckIcon('warning');
              }
            },
            error: e => {
              console.error('Oh no!', e);
              this.setHealthCheckIcon('warning');
            }
          });
        this.subs.push(dataLoadSub);
      },
      error: e => {
        this.setHealthCheckIcon('error');
      }
    });
  }

  getKeyIcon(key: string): string {
    const type = this.data!.getDataType(key);
    const value = this.data!.getFieldValue(key);
    switch (type) {
      case DataType.String:
        return 'text_fields';
      case DataType.Boolean:
        return value ? 'check_box' : 'check_box_outline_blank';
      case DataType.Integer:
        return 'dialpad';
      case DataType.Float:
        return 'calculate';
      default:
        console.warn('Data Type received', type);
        return 'warning';
    }
  }

  setHealthCheckIcon(icon: string): void {
    this.healthCheckIcon = icon;
  }

  get backendUrl(): string {
    return this.backendService.serverUrl;
  }
}
