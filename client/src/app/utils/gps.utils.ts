export interface TpvObject {
  class: string;
  /** Name of the originating device */
  device?: string;
  /** NMEA mode */
  mode: number;
  /** GPS fix status */
  status?: number;
  /**
   * Time/date stamp in ISO8601 format, UTC.
   * May have a fractional part of up to .001sec precision.
   * May be absent if the mode is not 2D or 3D.
   * May be present, but invalid, if there is no fix.
   * Verify 3 consecutive 3D fixes before believing it is UTC.
   * Even then it may be off by several seconds until the current leap seconds is known.
   */
  time?: string;
  /** Altitude, height above ellipsoid, in meters. Probably WGS84. */
  altHAE?: number;
  /**
   * MSL Altitude in meters.
   * The geoid used is rarely specified and is often inaccurate.
   * See the comments below on geoidSep. altMSL is altHAE minus geoidSep.
   */
  altMSL?: number;
  /** @deprecated Undefined. Use altHAE or altMSL. */
  alt?: number;
  /** Climb (positive) or sink (negative) rate, meters per second. */
  climb?: number;
  /** Current datum. Hopefully WGS84. */
  datum?: string;
  /** Age of DGPS data. In seconds */
  dgpsAge?: number;
  /** Station of DGPS data. */
  dgpsSta?: number;
  /** Estimated climb error in meters per second. Certainty unknown. */
  epc?: number;
  /** Estimated track (direction) error in degrees. Certainty unknown. */
  epd?: number;
  /** Estimated horizontal Position (2D) Error in meters. Also known as Estimated Position Error (epe). Certainty unknown. */
  eph?: number;
  /** Estimated speed error in meters per second. Certainty unknown. */
  eps?: number;
  /** Estimated time stamp error in seconds. Certainty unknown. */
  ept?: number;
  /** Longitude error estimate in meters. Certainty unknown. */
  epx?: number;
  /** Latitude error estimate in meters. Certainty unknown. */
  epy?: number;
  /** Estimated vertical error in meters. Certainty unknown. */
  epv?: number;
  /**
   * Geoid separation is the difference between the WGS84 reference ellipsoid
   * and the geoid (Mean Sea Level) in meters. Almost no GNSS receiver specifies
   * how they compute their geoid. gpsd interpolates the geoid from a 5x5 degree
   * table of EGM2008 values when the receiver does not supply a geoid separation.
   * The gpsd computed geoidSep is usually within one meter of the "true" value,
   * but can be off as much as 12 meters.
   */
  gepidSep?: number;
  /** Latitude in degrees: +/- signifies North/South. */
  lat?: number;
  /** Longitude in degrees: +/- signifies East/West. */
  lon?: number;
  /** Course over ground, degrees from true north. */
  track?: number;
  /** Course over ground, degrees magnetic. */
  magtrack?: number;
  /**
   * Magnetic variation, degrees.
   * Also known as the magnetic declination (the direction of the horizontal
   * component of the magnetic field measured clockwise from north) in degrees,
   * Positive is West variation. Negative is East variation.
   */
  magvar?: number;
  /** Speed over ground, meters per second. */
  speed?: number;
  /** Down component of relative position vector in meters. */
  velD?: number;
  /** East component of relative position vector in meters. */
  velE?: number;
  /** North component of relative position vector in meters. */
  velN?: number;
}

export declare type GpsFix = 'Unknown' | 'No' | '2D' | '3D';
export function parseGpsFix(fix: number | undefined): GpsFix {
  switch (fix) {
    case 1: return 'No';
    case 2: return '2D';
    case 3: return '3D';
    default: return 'Unknown';
  }
}

export function calculateDistance(
  coordA: [number, number],
  coordB: [number, number]
): number {
  return calcCrow(coordA[0], coordA[1], coordB[0], coordB[1]);
}

function calcCrow(lat1: number, lon1: number, lat2: number, lon2: number) {
  const R = 6371; // km
  const dLat = toRad(lat2 - lat1);
  const dLon = toRad(lon2 - lon1);
  const lat1_Rad = toRad(lat1);
  const lat2_Rad = toRad(lat2);

  const a = Math.sin(dLat / 2) * Math.sin(dLat / 2) +
    Math.sin(dLon / 2) * Math.sin(dLon / 2) * Math.cos(lat1_Rad) * Math.cos(lat2_Rad);
  const c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1 - a));
  const d = R * c;
  return d;
}

// Converts numeric degrees to radians
function toRad(Value: number) {
  return Value * Math.PI / 180;
}
