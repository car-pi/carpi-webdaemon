import { BACKEND_URL } from './backend.service';
import { Inject, Injectable, OnDestroy } from '@angular/core';
import { HubConnection, HubConnectionBuilder } from '@microsoft/signalr';
import { Observable, Subject, Subscription, interval, map, filter } from 'rxjs';
import { DataType, INewValue } from './entities';

@Injectable({
  providedIn: 'root'
})
export class SignalrValuesService implements OnDestroy {

  private readonly connection: HubConnection;
  private readonly valueSubject$: Subject<INewValue> = new Subject<INewValue>();
  private healthCheck: Subscription | undefined;

  constructor(
    @Inject(BACKEND_URL) private readonly baseUrl: string
  ) {
    this.connection = new HubConnectionBuilder()
      .withUrl(`${baseUrl}/carpi`)
      .build();
    this.setup();
  }

  private setup(): void {
    console.log('Setting up SignalR');
    this.connection.on('ValueReceived', (channel: string, stringValue: string) => {
      this.valueSubject$.next({
        channel: channel,
        stringValue: stringValue
      });
    });

    this.connect();
  }

  get hubConnection(): HubConnection {
    return this.connection;
  }

  get valueReceived$(): Observable<INewValue> {
    return this.valueSubject$;
  }

  subToField(type: DataType, field: string): Observable<string> {
    return this.valueReceived$.pipe(
      filter(v => v.channel === `${type}${field}`),
      map(v => v.stringValue)
    );
  }

  subToFloat(field: string): Observable<number | undefined> {
    return this.subToField(DataType.Float, field).pipe(
      map(v => Number.parseFloat(v))
    );
  }

  subToInt(field: string): Observable<number | undefined> {
    return this.subToField(DataType.Integer, field).pipe(
      map(v => Number.parseInt(field, 10))
    );
  }

  subToBoolean(field: string): Observable<boolean | undefined> {
    return this.subToField(DataType.Boolean, field).pipe(
      map(v => v === '1')
    );
  }

  ngOnDestroy(): void {
    this.healthCheck?.unsubscribe();
    this.connection.stop().then(() => {
      console.log('Disconnected');
    });
  }

  async connect(): Promise<void> {
    try {
      await this.connection.start();
      this.healthCheck = interval(1000).subscribe(async () => {
        try {
          await this.connection.send('Ping');
        } catch (e) {
          console.error('Failed to ping', e);
        }
      });
    } catch (e) {
      console.error('Failed to connect to signalr', e);
    }
  }
}
