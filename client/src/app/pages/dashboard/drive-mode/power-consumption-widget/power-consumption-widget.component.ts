import { Component, OnInit, ChangeDetectionStrategy, Input } from '@angular/core';
import { ProgressBarMode } from '@angular/material/progress-bar';

@Component({
  selector: 'app-power-consumption-widget',
  templateUrl: './power-consumption-widget.component.html',
  styleUrls: ['./power-consumption-widget.component.scss'],
  changeDetection: ChangeDetectionStrategy.OnPush
})
export class PowerConsumptionWidgetComponent implements OnInit {

  @Input() consumptionKw: number | null | undefined = undefined;
  @Input() maxChargeRateKw: number = 50;
  @Input() maxAvailableChargeRateKw: number | null | undefined = undefined;
  @Input() maxDischargeRateKw: number = 100;
  @Input() maxAvailableDischargeRateKw: number | null | undefined = undefined;
  @Input() ecoModeLimit: number = 50;

  constructor() { }

  ngOnInit(): void {
  }

  get hasPowerConsumption(): boolean {
    return this.consumptionKw !== undefined;
  }
  get batteryBarColor(): string {
    if (this.hasPowerConsumption && this.consumptionKw! > this.ecoModeLimit) {
      return 'accent';
    }
    return 'primary';
  }
  get batteryDischargeBarMode(): ProgressBarMode {
    if (this.hasPowerConsumption && this.consumptionKw! >= 0) {
      return 'determinate';
    } else {
      return 'buffer';
    }
  }
  get batteryChargeBarMode(): ProgressBarMode {
    if (this.hasPowerConsumption && this.consumptionKw! <= 0) {
      return 'determinate';
    } else {
      return 'buffer';
    }
  }

  get currentChargingRate(): number {
    return Math.abs(Math.min(this.consumptionKw!, 0)) / this.maxChargeRateKw * 100;
  }
  get maxChargingRate(): number {
    return (this.maxAvailableChargeRateKw || 0) / this.maxChargeRateKw * 100;
  }
  get currentDischargingRate(): number {
    return Math.abs(Math.max(this.consumptionKw!, 0)) / this.maxDischargeRateKw * 100;
  }
  get maxDischargeRate(): number {
    return (this.maxAvailableDischargeRateKw || 0) / this.maxDischargeRateKw * 100;
  }

}
